# 0.3

## 0.3.0 (2023-11-05)
   - New, lazy, API variant available in parallel to the old (greedy) one.
   - `CmdError`: Field `output_lines` is now `error_lines`
   - Remove `git.has_output()` method – use `git.first_line() != ""`
     instead.
   - Add `git_adapter.git.version` variable, so we can enforce a minimum:
     ```python
         from git_adapter.git import version as git_adapter_version
          assert git_adapter_version >= (0, 3)
     ```

# 0.2

## 0.2.5 (2020-09-28)
   - Reimplement environ mechanism (and this time use it).

## 0.2.4 (2020-09-17)
   - Start extracting a more general command interface.
   - Remove environ mechanism that was not used

## 0.2.3 (2020-07-26)
   - First usable version
